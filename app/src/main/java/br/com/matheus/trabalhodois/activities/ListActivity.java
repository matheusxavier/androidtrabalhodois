package br.com.matheus.trabalhodois.activities;

import androidx.appcompat.app.AppCompatActivity;
import br.com.matheus.trabalhodois.R;
import br.com.matheus.trabalhodois.entity.Mutant;
import br.com.matheus.trabalhodois.entity.MutantCell;
import br.com.matheus.trabalhodois.services.MutantsRest;
import br.com.matheus.trabalhodois.services.VolleyCallback;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {

    private ArrayList<Mutant> mutants;
    private MutantsRest mutantsRest;
    @BindView(R.id.list) ListView _listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);

        mutants = new ArrayList<Mutant>();

        _listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Mutant selectedMutant = mutants.get(position);
                Intent intent = new Intent(ListActivity.this, MutantDetail.class);
                intent.putExtra("mutant", selectedMutant);
                startActivity(intent);
            }
        });


        mutantsRest = new MutantsRest();
        this.getList();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    private void getList() {
        final ProgressDialog progressDialog = new ProgressDialog(ListActivity.this, R.style.AppTheme);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Carregando seus mutantes...");
        progressDialog.show();

        mutantsRest.list(new VolleyCallback() {
            @Override
            public void onSuccess(Integer code, String jsonResponse) {
                progressDialog.dismiss();
                onListSuccess(jsonResponse);
            }

            @Override
            public void onError(int code) {
                progressDialog.dismiss();
                Log.d("Erro", ""+code);
            }
        }, this);
    }

    private void onListSuccess(String json) {
        try {
            Gson gson = new Gson();
            JSONObject object = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(object.get("mutants").toString());

            ArrayList<String> mutantsNames = new ArrayList<String>();
            ArrayList<String> mutantsPictures = new ArrayList<>();

            for (int i = 0; i < jsonArray.length(); i++) {
                Mutant model = gson.fromJson(jsonArray.get(i).toString(), Mutant.class);
                mutants.add(model);
                mutantsNames.add(model.getName());
                mutantsPictures.add("http://192.168.43.3:8000" + model.getImagePath());
            }

            MutantCell adapter = new MutantCell(ListActivity.this, mutantsNames, mutantsPictures);
            _listView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("Erro", e.getMessage());
        }
    }
}
