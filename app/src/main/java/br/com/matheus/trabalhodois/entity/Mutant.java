package br.com.matheus.trabalhodois.entity;

import java.io.Serializable;
import java.util.ArrayList;

public class Mutant implements Serializable {

    int id;
    String name;
    String image_path;
    ArrayList<Ability> abilities;

    public Mutant(String name) {
        this.name = name;
        this.abilities = new ArrayList<Ability>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImagePath() {
        return image_path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setImagePath(String path) {
        this.image_path = path;
    }

    public ArrayList<Ability> getAbilities() {
        return abilities;
    }

    public void setAbilities(ArrayList<Ability> abilities) {
        this.abilities = abilities;
    }

    public void addAbility(Ability ability) {
        this.abilities.add(ability);
    }
}
