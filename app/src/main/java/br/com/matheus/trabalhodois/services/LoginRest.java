package br.com.matheus.trabalhodois.services;

import android.app.Activity;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.Listener;
import com.android.volley.Response.ErrorListener;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.matheus.trabalhodois.entity.Login;

public class LoginRest {

    private String URL = "auth/login/";

    public void login(final VolleyCallback callback, final Activity activity, Login login) {
        Gson gson = new Gson();
        String json = gson.toJson(login);

        try {

            JSONObject jsonObject = new JSONObject(json);

            final JsonObjectRequest jsonRequest = new UnsecureRequest(Request.Method.POST, URL, jsonObject,
                    new Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                callback.onSuccess(200, response.getString("access_token"));
                            } catch (JSONException e) {
                                callback.onError(10);
                            }
                        }
                    },
                    new ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if(error.networkResponse != null)
                                callback.onError(error.networkResponse.statusCode);
                            else
                                callback.onError(10);
                        }
                    }
            ) {
                @Override
                protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                    try {
                        JSONObject jsonResponse = new JSONObject();
                        jsonResponse.put("access_token", response.headers.get("Authorization"));

                        return Response.success(jsonResponse, HttpHeaderParser.parseCacheHeaders(response));
                    } catch (JSONException e) {
                        return Response.error(new ParseError(e));
                    }
                }
            };

            SingletonRequestQueue.getInstance(activity).addToRequestQueue(jsonRequest);
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d("Erro", "Erro ao converter objeto para JSON");
        }
    }

}
