package br.com.matheus.trabalhodois.activities;

import android.content.Intent;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import br.com.matheus.trabalhodois.R;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                showLogin();
            }
        }, 3000);
    }

    private void showLogin() {
        Intent it = new Intent(SplashScreen.this, LoginActivity.class);
        startActivity(it);
        finish();
    }
}
