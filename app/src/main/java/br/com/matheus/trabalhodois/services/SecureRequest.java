package br.com.matheus.trabalhodois.services;

import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SecureRequest extends JsonObjectRequest {

    public Activity context;
    private static final String BASE_URL = "http://192.168.43.3:8000/api/";

    public SecureRequest(int method, String url, JSONObject jsonRequest, Activity activity, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(method, BASE_URL + url, jsonRequest, listener, errorListener);
        this.context = activity;
    }

    public SecureRequest(int method, String url, Activity activity, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(method, BASE_URL + url, null, listener, errorListener);
        this.context = activity;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        TokenManagement tokenManagement = new TokenManagement(context);
        String token = tokenManagement.getToken();

        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("Authorization", "Bearer " + token);
        return headers;
    }


}
