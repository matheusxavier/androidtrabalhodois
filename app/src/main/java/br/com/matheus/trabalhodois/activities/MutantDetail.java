package br.com.matheus.trabalhodois.activities;

import androidx.appcompat.app.AppCompatActivity;
import br.com.matheus.trabalhodois.R;
import br.com.matheus.trabalhodois.entity.Mutant;
import br.com.matheus.trabalhodois.services.MutantsRest;
import br.com.matheus.trabalhodois.services.VolleyCallback;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.InputStream;

public class MutantDetail extends AppCompatActivity {

    private Mutant mutant;
    @BindView(R.id.abilities) LinearLayout _abilitiesContainer;
    @BindView(R.id.mutantName) TextView _mutantName;
    @BindView(R.id.img) ImageView _mutantImage;

    MutantsRest mutantsRest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mutant_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ButterKnife.bind(this);

        mutantsRest = new MutantsRest();

        Intent it = getIntent();
        mutant = (Mutant) it.getSerializableExtra("mutant");

        this.fillScreen(mutant);
        new DownloadImageTask(_mutantImage).execute("http://192.168.43.3:8000" + mutant.getImagePath());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    private void fillScreen(Mutant mutant) {
        _mutantName.setText(mutant.getName());

        for (int i = 0; i < mutant.getAbilities().size(); i++) {
            TextView textView = new TextView(this);
            textView.setText(mutant.getAbilities().get(i).getName());
            textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            textView.setTextSize(25);
            _abilitiesContainer.addView(textView);
        }
    }

    public void deleteMutant(View view) {
        mutantsRest.delete(new VolleyCallback() {
            @Override
            public void onSuccess(Integer code, String message) {
                onDeleteSuccess();
            }

            @Override
            public void onError(int code) {
                Log.d("Erro", "Erro ao excluir mutante");
            }
        }, this, mutant);
    }

    private void onDeleteSuccess() {
        Log.d("Excluiu", "Excluiu mutante com sucesso.");
        Toast.makeText(this, mutant.getName() + " excluído com sucesso!", Toast.LENGTH_SHORT).show();
        onBackPressed();
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}
